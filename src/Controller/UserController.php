<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\UserRepository;

class UserController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/api/user", name="user", methods={"GET"})
     */
    public function index(UserRepository $userRepository)
    {
        return $userRepository->findAll();
    }

    /**
     * @Route("/api/user/new", name="user_new", methods={"POST"}),
     * @example({"email":"test123@temp.com","roles":"['USER_ADMIN']", "password":"test123"})
     */
    public function create(Request $request, UserPasswordHasherInterface $userPasswordHasher): array
    {
        $user = new User();

        $user->setEmail($request->get('email'));
        $user->setRoles([$request->get('roles')]);
        $user->setPassword($userPasswordHasher->hashPassword($user, $request->get('password')));

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return ['status' => 'success'];
    }

    /**
     * @Route("/api/user/{id}", name="user_show", requirements={"id" = "\d+"}, methods={"GET"})
     */
    public function show(UserRepository $userRepository, $id)
    {
        return $userRepository->findOneById($id);
    }

    /**
     * @Route("/api/user/{id}/edit", name="user_edit", requirements={"id" = "\d+"}, methods={"PUT"})
     * @example({"email":"test123@temp.com","roles":"['USER_ADMIN']", "password":"test123"})
     */
    public function edit(Request $request, UserPasswordHasherInterface $userPasswordHasher, $id): array
    {
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['id' => $id]);

        $user->setEmail($request->get('email') !== null ? $request->get('email') : $user->getEmail());
        $user->setRoles($request->get('roles') !== null ? [$request->get('roles')] : $user->getRoles());
        $request->get('password') ? $user->setPassword($userPasswordHasher->hashPassword($user, $request->get('password'))) : null;

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return ['status' => 'success'];
    }

    /**
     * @Route("/api/user/{id}", name="user_delete", requirements={"id" = "\d+"}, methods={"DELETE"})
     */
    public function delete($id): array
    {
        $this->entityManager->remove($this->entityManager->getRepository(User::class)->findOneBy(['id' => $id]));
        $this->entityManager->flush();

        return ['status' => 'success'];
    }
}